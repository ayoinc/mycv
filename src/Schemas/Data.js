export const Data = {
  profile: {
    name: "Ayodele Ajayi",
    ocupation: "DevSecOps Engineer",
    location: "Kent, United Kingdom",
    email: "ayoinc@gmail.com",
    telephone: "+447758163391",
    image: "images/1631187039417.jpeg",
    web: "https://linkedin.com/in/ayoinc/"
  },
  aboutMe: {
    label: "Profile",
    description:
      "A bright, talented and ambitious SRE/DevOps/SysOps / Security / Application specialist with a strong technical background who possesses self-discipline and the ability to work with the minimum of supervision. Having exposure to a wide range of technologies & able to play a key role in diagnosing hardware, software problems, analyse network logs for security and to ensure that quality solutions meet business objectives. Possessing a good team spirit, deadline orientated and having the ability to organize and present complex solutions clearly and accurately.",
  },
  skills: {
    technicalLabel: "Tech Stack",
    softLabel: "Skills",
    technicalSkills: [
      "Docker",
      "Kubernetes",
      "Git",
      "Python",
      "IAC",
      "Terraform",
      "Cloudformation",
      "CoPilot",
      "ReactJS",
      "API",
      "SQL",
      "NOSQL",
      "Git",
      "CI/CD",
      "AWS",
      "Azure",
      "Linux",
      "Azure",
      "GCP",
    ],
    softSkills: [
      "Cost Management",
      "Collaboration",
      "Web Development",
      "Deployment Strategies",
      "Communication",
      "Creativity",
      "Proactivity",
      "Mentoring",
    ],
  },
  socialMedia: {
    label: "SOCIAL",
    social: [
      {
        label: "Linkedin",
        name: "linkedin",
        url: "https://linkedin.com/in/ayoinc/",
        className: "bxl-linkedin-square",
      },
      {
        label: "Github",
        name: "github",
        url: "https://github.com/ayoinc",
        className: "bxl-github",
      },
      // {
      //   label: "Maaato",
      //   name: "twiiter",
      //   url: "https://twitter.com/I_Maaato",
      //   className: "bxl-twitter",
      // },
    ],
  },
  experience: {
    works: [
      {
        title: "DevSecOps Engineer",
        period: "July. 2022 - Date",
        company: "Vault Platform",
        description: [
          "Looking after the DevOps process, including the creation of CI/CD pipelines, automated testing, and continuous deployment.",
          "Implementation of automated testing pipelines, including the creation of test suites, test cases, and test automation scripts.",
          "Building dashboard and reporting of the automation testing process.",
          "Monitoring and reporting of production deployments.",
          "Planning and materialising Dockerisation in Cloud.",
        ],
      },
      {
        title: "DevOps Engineer",
        period: "Dec. 2021 - June 2022",
        company: "eConsult Health",
        description: [
          "Responsible for implementation and championing of the DevOps process, including the creation of CI/CD pipelines, automated testing, and continuous deployment.",
          "Implementation of automated testing pipelines, including the creation of test suites, test cases, and test automation scripts.",
          "Building dashboard and reporting of the automation testing process.",
          "Monitoring and reporting of production deployments.",
          "Using Docker to containerise with AWS integration enabled features.",
        ],
      },
      {
        title: "DevOps/Platform Engineer",
        period: "March 2021 - December 2021",
        company: "Doctor Care Anywhere",
        description: [
          "Azure Cloud Platform (AKS) deployment of the Doctor Care Anywhere platform.",
          "Implementation of CI/CD pipelines, automated testing, and continuous deployment.",
          "Implementation of automated testing pipelines, including the creation of test suites, test cases, and test automation scripts.",
          "Maintenance of the platform and its infrastructure.",
          "Collaboration with the product team to ensure that the platform is up to date and stable.",
        ],
      },
      {
        title: "Site Reliability Engineer",
        period: "March 2020 - March 2021",
        company: "Rank Group",
        description: [
          "Implementation of CI/CD pipelines, automated testing, and continuous deployment.",
          "Implementation of automated testing pipelines, including the creation of test suites, test cases, and test automation scripts.",
          "Maintenance of the platform and its infrastructure.",
          "Collaboration with the product team to ensure that the platform is up to date and stable.",
          "Docker, Kubernetes, and Jenkins to automate the testing process.",
        ],
      },
      {
        title: "DevOps Engineer",
        period: "Aug 2019 - Mar 2020",
        company: "Adjoint Inc.",
        description: [
          "Implementation of CI/CD pipelines, automated testing, and continuous deployment.",
          "Implementation of automated testing pipelines, including the creation of test suites, test cases, and test automation scripts.",
          "Maintenance of the platform and its infrastructure.",
          "Collaboration with the product team to ensure that the platform is up to date and stable.",
          "GKE, Docker, and GitOps to orchestrate and automate the CI/CD process.",
          "Containerization of the platform.",
        ],
      },
      // {
      //   title: "DevOps Consultant",
      //   period: "July. 2019 - Date",
      //   company: "ContainerCode Club",
      //   description: [
      //     "Giving recommendations, planning and creating secure and effective DevOps process for customers",
      //   ],
      // },
      {
        title: "Customer Support Engineer",
        period: "Jan 2018 - Aug 2019",
        company: "Axomic Limited",
        description: [
          "Support and IT problems resolving for Digital Asset Management",
          "Worked with the customer to resolve their issues and provide solutions.",
          "Provided technical support to the customer.",
          "Writing technical documentation for the customer.",
          "Creating proof of concepts for product improvements using various Tech Stack.",
          "Sorting out data with Jupyter Notebooks.",
          "Using Python, Pandas, Numpy, and Matplotlib to create visualizations.",
          "Containerizing deliverable proof of concepts.",
        ],
      },
      {
        title: "System Analyst",
        period: "Mar 2016 - Jan 2018",
        company: "Indivior Plc.",
        description: [
          "Support and IT problems resolving for Pharmaceuticals",
          "Worked with the customer to resolve their issues and provide solutions.",
          "Provided technical support to the customer.",
          "Writing technical documentation for the customer.",
        ],
      },
    ],
    academic: [
      {
        career: "Cybersecurity: Managing Risk in the Information Age",
        date: "2018",
        institution: "Harvard University",
      },
      {
        career: "Cyber Security for Small and Medium Businesses",
        date: "2016",
        institution: "The Open University",
      },
      {
        career: "PRINCE2 (Projects In Controlled Environments 2)",
        date: "2016",
        institution: "Firebrand Academy",
      },
    ],
    proyects: [
      {
        name: "New Deployment Strategy for the platform",
        company: "Doctor Care Anywhere",
        period: "2020 - 2021",
        description: [
          "Created a strategy and CI/CD pipeline using GitOps (Flux) and Azure DevOps to automate the deployment of the Azure Kubernestes platform",
        ],
      },
      {
        name: "Powerful GitOps Workflow for the platform",
        company: "Adjoint Inc.",
        period: "2019 - 2020",
        description: [
          "Created a CI/CD pipeline using GitOps (Flux) and GitHub Actions to automate the deployment of the GKE platform",
        ],
      },
      {
        name: "Client Migration",
        company: "OpenAsset",
        period: "2018 - 2019",
        description: [
          "Migration of Customer OnPremises Licensing Systems to AWS Cloud using Ruby on Rails and REST APIs",
        ],
      },
      {
        name: "Demerger from Parent-Child Relationship",
        company: "Indivior PLC",
        period: "2015 - 2016",
        description: [
          "Migration of Active Directory Users to Isolated Server",
          "Building a new Identity Management System",
          "Automating hardened Golden Images for machine deployment",
          "Migration from Windows 7 to Windows 10",
        ],
      },
    ],
  },
};
