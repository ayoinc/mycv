FROM node:16-alpine
WORKDIR /app
COPY . .
RUN npm install --global serve
RUN npm install --legacy-peer-deps && npm run build
EXPOSE 8080
ENTRYPOINT [ "serve", "--single", "--no-clipboard", "--listen", "8080", "/app/build" ]